class apache_redirect::install {
  class { 'apache':    
    default_mods => false, 
  }  
  
  include apache::mod::proxy  

  apache::vhost { "*:$apache_redirect::params::port":    
    servername          => 'localhost',  
	  serveraliases       => ['*'],
    port                => $apache_redirect::params::port,    
    docroot             => '/var/www/webserver',    
    proxy_preserve_host => 'true',
    proxy_dest          => "http://localhost:$apache_redirect::params::redirect_port",
  }  
}
class java::install {
	package { $java::params::jre:
    	ensure => 'latest'
	}
}
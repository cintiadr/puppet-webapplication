class puppet_service::install {
  cron { 'puppet':
    ensure  => 'present',
    command => $puppet_service::params::command,
    user    => 'root', 
    minute  => '*/10', 
  }
}

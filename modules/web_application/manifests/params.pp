class web_application::params{

	$url = 'https://s3-ap-southeast-2.amazonaws.com/covata-test/application.jar'

	$application_directory = '/opt/web-application'
	$application_filename  = 'covata-web-application.jar'
	$version_filename      = 'version.txt'
	$application_path      = "$application_directory/$application_filename"
	$service_name          = 'covata-web-application'
	$service_path          = '/etc/init.d/covata-web-application'

	$app_version_cmd       = "wget -S --spider $url 2>&1 -t 10 | fgrep x-amz-version-id "
	$retrieve_jar_cmd      = "wget $url -O $application_path -t 10"
	$retrieve_version_cmd  = "$app_version_cmd > $application_directory/$version_filename"
	$compare_version_cmd   = "$app_version_cmd > /tmp/version.txt; diff /tmp/version.txt $application_directory/$version_filename >/dev/null 2>&1"


	$user                  = 'web-application'
    $group                 = 'web-application'
}
class web_application::config{

  group { $web_application::params::group:
    ensure => 'present',
  }

  user { $web_application::params::user:
    ensure     => 'present',
    gid        => $web_application::params::group,
    managehome => true,
  }

  file { $web_application::params::application_directory:
    ensure => 'directory',
    owner  => $web_application::params::user,
    group  => $web_application::params::group,
    mode   => 755,
  }

  file { $web_application::params::service_path:
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => 755,
    content => template("web_application/etc/init.d/covata-web-application.erb")
  }  

  exec { $web_application::params::application_path:
    command     => "$web_application::params::retrieve_jar_cmd; 
                    $web_application::params::retrieve_version_cmd",
    unless      => "$web_application::params::compare_version_cmd",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/opt/local/bin:/usr/sfw/bin',
    notify      => Service[$web_application::params::service_name],
    require     => File[$web_application::params::application_directory],
  }
}

class web_application::install{
	require web_application::config
	require java

	service { $web_application::params::service_name:
      ensure   => running,
      enable   => true,
    }
}
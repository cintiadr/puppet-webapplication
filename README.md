## Overview

This repository contains the puppet and shell scripts to provision a machine with a proxy on port 80 to another application running on port 8080. The application is retrieved from a S3 bucket. Puppet will run masterless, scheduled. 

You have two choices to test these manifests; one is running Vagrant (```vagrant up```), the other is creating a new Ubuntu instance in Amazon and run the ```setup-machines-helper/setup-puppet-and-dependencies-from-repository``` during the startup. Something like:
```
bash <(curl -fsSL https://bitbucket.org/cintiadr/puppet-webapplication/raw/master/setup-machines-helper/setup-puppet-and-dependencies-from-repository)

```
Ask me for S3 deployment and EC2 access. 


## Deploying a new version of the application

### Configuring your environment

```
$ aws configure
AWS Access Key ID [****************]: 
AWS Secret Access Key [****************]: 
Default region name []: us-east-1
Default output format [json]: json

# Getting the code
$ git clone git@github.com:spring-projects/spring-boot.git

$ cd spring-boot/spring-boot-samples

```


### Deploying a new version of the application

```
#edit the output
$ vim spring-boot-sample-jetty/src/main/java/sample/jetty/service/HelloWorldService.java 

$ mvn -pl spring-boot-sample-jetty/ -am package -DskipTests && cp spring-boot-sample-jetty/target/spring-boot-sample-jetty-1.3.0.BUILD-SNAPSHOT.jar /tmp/application.jar

$ aws s3 cp /tmp/application.jar s3://covata-test/ 
```

Wait up to 10 minutes for the puppet run (or force it running the ```/etc/puppet/puppet-helper/update-run-puppet-and-dependencies```). In Amazon it takes about 1 minute to get the 503 error gone after the restart, don't panic. 

### Logs
  - Puppet logs (with git pull and puppet librarian): ```/var/logs/puppet_logs```
  - Application logs: ```/home/web-application/web-application_logs```

## Design decisions

  - I'm using Ubuntu LTS 14.04. 
  - I'm running librarian puppet to retrieve puppet dependencies; this is usually done outside vagrant - on the host itself. But I'm doing it from inside vagrant because it prevents installing librarian puppet requirements on the host machine. Effectively it's the same, as the folder is shared between host and guest. 
  - Librarian puppet & puppet dependencies are installed in ```setup-machines-helper/setup-dependencies```. Same script is used in production and vagrant. I could use bundler instead of installing the two gems, it looked like exactly same complexity. Ideally this script should be run only when creating the basebox.  
  - Puppet is running masterless; I added a simple cron job to rerun it. This git repository replaces ```/etc/puppet``` folder, and it needs to be populated with the git repository on startup. I haven't created a hiera data as I don't have a clear view of all the environments and there was no data to share across the different modules. 
  - You will notice I haven't set authorized_keys nor firewall. I'm using Amazon for production, and I'm setting those two things from there only. 
  - I made the application a simple init service. I believe it would eventually become a daemon somehow, with multilog and everything, but I didn't bother doing it now. 
  - I used apache for the simple reason that I knew it a little better than others, and its puppet module is very powerful and supported; nginx module was pretty basic. 
  - I'm running puppet as root, but the application itself has a user specifically for it. My thinking is that puppet will modify everything as root, but the application should be contained as a simple user. 
  - One of my biggest concerns was how to create a good pact between ops and devs, enabling continuous delivery. I wanted a clear definition of what will be deployed, and having a fast feedback. More important, I don't like deploying applications with puppet; when it fails it's hard to tell if it was an infra or application change, devs will be waiting for asynchronous deployments. I believe puppet is awesome to handle infra, but a deployment must be made by CI. It could use capistrano, mcollective, CodeDeploy or anything to execute it; the feedback loop is much better. Anyway, I don't have a CI for this exercise, so I went for puppet to install on every version. 
  - That said, I need somewhere to store the new version of the application. It's very common to see people using deb packages for that, letting puppet retrieve the latest version. I don't like debian packages for deployments, I think they make it too easy to do the wrong thing, and it's a simple step to get debian packages installing dependencies and other hard core infra changes. I'd use nexus if I had it, or some any other FTP/NFS/EFS file repository easily accessible. I just delegated this part to Amazon, using a public versioned S3 bucket. When there's a new version deployed, it will be downloaded and the service restarted. 
  - I didn't set any tests. If more development is expected in this box, cucumber could be set up. Monitoring puppet running and application health check would be done in other server. 

